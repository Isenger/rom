import java.io.*;
import java.util.Scanner;
import java.util.ArrayList;


public class Inventory
{
    ArrayList<ItemGame> slot = new ArrayList<ItemGame>();
    int fully = 20;
    int usage = 0;
    int i = 0;

    public void pickUpItem(){
        int random = (int)(Math.random()*4);
        //Scanner select = new Scanner(System.in);
        ItemGame itemDrop = new ItemGame();
        itemDrop = itemDrop.addItemGame(random);
        if(usage < fully){
            slot.add(itemDrop);
            usage++;
        }
        else
            System.out.println("Inventory Full");
    }
    public int pickPotion(){
        int potion = 0;
        for(ItemGame item: slot){
            if(item.name.startsWith("Potion")){
                potion++;
            }
        }
        return potion;
    }
    public int pickWeapon(){
        int weapon = 0;
        for(ItemGame item: slot){
            if(item.name.startsWith("Weapon")){
                weapon++;
            }
        }
        return weapon;
    }
    public int pickAmror(){
        int amror = 0;
        for(ItemGame item: slot){
            if(item.name.startsWith("Amror")){
                amror++;
            }
        }
        return amror;
    }
    public void usePotion(){
        int usePotion = 0; 
        for(int i = 0 ; i < fully ; i++){
            usePotion = slot.get(i).name.indexOf("Potion");
            if(usePotion != -1)
                break;
        }
        slot.remove(usePotion);
        usage--;
    }
    public void useWeapon(){
        int useWeapon = 0; 
        for(int i = 0 ; i < fully ; i++){
            useWeapon = slot.get(i).name.indexOf("Weapon");
            if(useWeapon != -1)
                break;
        }
        slot.remove(useWeapon);
        usage--;
    }
    public void useAmror(){
        int useAmror = 0; 
        for(int i = 0 ; i < fully ; i++){
            useAmror = slot.get(i).name.indexOf("Amror");
            if(useAmror != -1)
                break;
        }
        slot.remove(useAmror);
        usage--;
    }
    public void item(){
        System.out.println("!!!List Item!!!");
        System.out.println("[1] Weapon : " + pickWeapon());
        System.out.println("[2] Amror : " + pickAmror());
        System.out.println("[3] HpPotion : " + pickPotion());
        System.out.println("[4] MpPotion : "+ pickPotion());
    }
}