import java.io.*;
import java.util.Scanner;
import java.util.ArrayList;

public class Monster{
    public int damage;
    public int hp;
    String name;
    
    public Monster action(int num){
        Monster newMon = new Monster();
        if(num == 0){
            newMon.name = "Slime";
            newMon.hp = 10;
            newMon.damage = 1;
        }
        else if(num == 1){
            newMon.name = "Elec";
            newMon.hp = 200;
            newMon.damage = 60;
        }
        else if(num == 2){
            newMon.name = "Network";
            newMon.hp = 350;
            newMon.damage = 40;
        }
        else if(num == 3){
            newMon.name = "Static";
            newMon.hp = 500;
            newMon.damage = 100;
        }
    return   newMon;
    }  
}
